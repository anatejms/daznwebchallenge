const baseUrl =  "http://api.themoviedb.org/3";

/**
 * service to search in themoviedb API
 * https://developers.themoviedb.org/3
 */
class movieApi {

  /**
   * movieApi constructor
   * @param apiKey - api_key for themoviedb API
   */
  constructor(apiKey){
    this.apiKey = apiKey;
  }

  /**
   * performs a fetch call to themoviedb search movie endpoint
   * https://developers.themoviedb.org/3/search/search-movies
   * @param query - search query
   * @param success - success callback
   * @param fail - fail callback
   */
  search(query, success, fail){
    const url = `${baseUrl}/search/movie?api_key=${this.apiKey}&query=${encodeURIComponent(query)}`;

    window.fetch(url).then((r) => {
      success(r);
    }, () => {
      fail();
    })
  }
}

export default movieApi;