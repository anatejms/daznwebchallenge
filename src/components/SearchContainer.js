import React, { Component } from 'react';

import { SearchBar } from './SearchBar';
import { MovieList } from './MovieList';
import movieApi from '../services/movieApi';

/**
 * component SearchContainer
 * renders SearchBar and MovieList components
 */
export class SearchContainer extends Component {
  constructor(props){
    super(props);
    this.state = {
      movies: [],
      searchQuery: ""
    };
    //create movie API service with proper api_key
    this.movieApi = new movieApi("67b90ccb311329c95a45eb71fe4af321");
    this.search = this.search.bind(this);
  }

  search(query){
    this.setState({
      searchQuery: query
    });

    this.movieApi.search(query, (r) => {
      //success get json from fetch Response
      // and set it to state movies collection
      r.json().then((d) => {
        this.setState({
          movies: d.results
        });
      });
    }, () => {
      //failed to load
    });
  }

  render() {
    return (
      <div>
        <h1>DAZN Web Challenge</h1>
        <SearchBar onSubmit={this.search}/>
        <MovieList movies={this.state.movies}/>
      </div>
    );
  }
}