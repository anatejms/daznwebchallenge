import React, { Component } from 'react';
import PropTypes from 'prop-types';

/**
 * component SearchBar
 * renders search input and button for sending title to parent
 */
export class SearchBar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      title: ''
    };

    this.setTitle = this.setTitle.bind(this);
    this.handleClick = this.handleClick.bind(this);
  }

  setTitle(event) {
    //set state title based on input value
    this.setState({title: event.target.value});
  }

  handleClick() {
    //call props onSubmit with movie title
    this.props.onSubmit(this.state.title);
  }
  
  render() {
    return (
      <div>
        <input placeholder="movie title" value={this.state.title} onChange={this.setTitle}/>
        <button onClick={this.handleClick}>Search</button>
      </div>
    );
  }
}

SearchBar.propTypes = {
  onSubmit: PropTypes.func.isRequired
};