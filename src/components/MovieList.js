import React, { Component } from 'react';

/**
 * component MovieList
 * renders list of provided movies from parent
 */
export class MovieList extends Component {
  render() {
    return (
      <ul>
        {this.props.movies.map((item, index) => (
          <li key={index}>
            <b>{item.title}</b>
            <span>{item.overview}</span>
          </li>
        ))}
      </ul>
    );
  }
}