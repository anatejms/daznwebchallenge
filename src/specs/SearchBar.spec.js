import React from 'react';
import { expect } from 'chai';
import { shallow, mount } from 'enzyme';
import { SearchBar } from '../components/SearchBar';
import { spy } from 'sinon';

describe('SearchBar', () => {
  it('should render input and button', () => {
    //fake onSubmit function - since it's required
    const onSubmit = () => {};
    const wrapper = shallow(<SearchBar onSubmit={onSubmit}/>);
    expect(wrapper.containsAllMatchingElements([
      <input/>,
      <button>Search</button>
    ])).to.equal(true);
  });

  it('should accept movie title', () => {
    //fake onSubmit function - since it's required
    const onSubmit = () => {};
    const wrapper = mount(<SearchBar onSubmit={onSubmit}/>);
    const input = wrapper.find('input');
    const movieTitle = "Matrix";

    //set SearchBar input value to movieTitle
    input.simulate('change', {target: { value: movieTitle }});
    expect(wrapper.state('title')).to.eql(movieTitle);
  });

  it('should call onSubmit when Search button is clicked', () => {
    const addItemSpy = spy();
    const wrapper = shallow(<SearchBar onSubmit={addItemSpy}/>);
    const movieTitle = "Matrix";

    //set state title to MovieTitle
    wrapper.setState({title: movieTitle});

    const addButton = wrapper.find('button');

    //simulate click on search button
    addButton.simulate('click');

    expect(addItemSpy.calledOnce).to.equal(true);
    expect(addItemSpy.calledWith(movieTitle)).to.equal(true);
  });
});