import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import { SearchContainer } from '../components/SearchContainer';
import { SearchBar } from '../components/SearchBar';
import { MovieList } from '../components/MovieList';
import sinon from 'sinon';
import movieApi from '../services/movieApi';
import fetchHelper from './fetch-helper';

describe('SearchContainer', () => {
  fetchHelper();
  it('should render SearchBar and MovieList', () => {
    const wrapper = shallow(<SearchContainer/>);
    expect(wrapper.containsAllMatchingElements([
      <SearchBar/>,
      <MovieList/>
    ])).to.equal(true);
  });

  it('should start with empty movie list and empty searchQuery', () => {
    const wrapper = shallow(<SearchContainer/>);
    expect(wrapper.state('movies')).to.eql([]);
    expect(wrapper.state('searchQuery')).to.eql("");
  });

  it('should perform search', () => {
    const wrapper = shallow(<SearchContainer/>);
    const searchQuery = 'test';
    const api = new movieApi("api_key");

    //create a stub for search function and inject it into SearchContainer instance
    const stubSearch = sinon.stub(api, 'search');
    wrapper.instance().movieApi = api;

    //perform search
    wrapper.instance().search(searchQuery);


    expect(wrapper.state('searchQuery')).to.eql(searchQuery);
    expect(stubSearch).to.have.been.called;
  });

  it('should pass a bound search function to SearchBar', () => {
    const wrapper = shallow(<SearchContainer/>);
    const searchBar = wrapper.find(SearchBar);
    const api = new movieApi("api_key");

    //create a stub for search function and inject it into SearchContainer instance
    const stubSearch = sinon.stub(api, 'search');
    wrapper.instance().movieApi = api;

    //perform onSubmit prop call
    searchBar.prop('onSubmit')("test");

    expect(wrapper.state('searchQuery')).to.be.eql("test");
    expect(stubSearch).to.have.been.called;

  });
});