import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import { MovieList } from '../components/MovieList';

describe('MovieList', () => {
  it('should render empty list', () => {
    const wrapper = shallow(<MovieList movies={[]}/>);
    expect(wrapper.find('li')).to.have.length(0);
  });

  it('should render movies', () => {
    const movies = [{
      title: "movie1",
      overview: "overview1"
    },{
      title: "movie2",
      overview: "overview2"
    }];
    const wrapper = shallow(<MovieList movies={movies}/>);
    expect(wrapper.find('li')).to.have.length(2);
  });
});