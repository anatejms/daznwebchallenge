import sinon from 'sinon';

/**
 * reusable mocks for API
 */
export default () => {
  //mock window fetch for non browser testing
  window.fetch = () => {};

  //mock for success API response
  const mockApiResponse = () => {
    return new Promise((resolve) => {
      resolve("success");
    });
  };

  beforeEach(() => {
    sinon.stub(window, 'fetch');
    window.fetch.returns(Promise.resolve(mockApiResponse()));
  });

  afterEach(() => {
    window.fetch.restore();
  });
};