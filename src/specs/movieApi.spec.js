import chai from 'chai';
import sinon from 'sinon';
import sinonChai from 'sinon-chai';
import movieApi from '../services/movieApi';

import fetchHelper from './fetch-helper';

const expect = chai.expect;

chai.use(sinonChai);

describe('movie API', () => {
  fetchHelper();

  //mock for fail API response
  const mockApiResponseFail = () => {
    return new Promise((resolve, reject) => {
      reject();
    });
  };

  it('should accept apiKey in constructor and set to apiKey property', () => {
    const apiKey = "test123";
    const api = new movieApi(apiKey);

    expect(api.apiKey).to.be.equal(apiKey);
  });

  it('search should call request with proper url', () => {
    const apiKey = "test123";
    const api = new movieApi(apiKey);
    const url = "/search/movie?api_key=test123&query=test";
    const query = "test";

    api.search(query, () => {}, () => {});

    expect(window.fetch.calledWithMatch(url));
  });

  it('search should call callback if request is successful', () => {
    const apiKey = "test123";
    const api = new movieApi(apiKey);
    const query = "test";

    const callbackSpy = sinon.spy();

    api.search(query, callbackSpy);


    // search is async, need to wait for response to test calls
    setTimeout(function () {
      expect(callbackSpy).to.have.been.called;
      expect(callbackSpy).to.have.been.calledWith("success");
    }, 0);
  });

  it('search should call fail function if request is fails', () => {
    const apiKey = "test123";
    const api = new movieApi(apiKey);
    const query = "test";

    const failSpy = sinon.spy();

    window.fetch.returns(Promise.resolve(mockApiResponseFail()));

    api.search(query, () => {}, failSpy);


    // search is async, need to wait for response to test calls
    setTimeout(function () {
      expect(failSpy).to.have.been.called;
    }, 0);
  });

});