import React from 'react';
import ReactDOM from 'react-dom';

import { SearchContainer } from './src/components/searchContainer';

ReactDOM.render(<SearchContainer/>, document.querySelector('#root'));